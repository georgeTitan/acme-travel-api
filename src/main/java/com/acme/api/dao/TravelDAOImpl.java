package com.acme.api.dao;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.acme.api.domain.entity.Flight;
import com.acme.api.message.Destination;


public class TravelDAOImpl implements TravelDAO {
	
	@PersistenceContext(unitName = "travelPersistenceUnit")
	private final EntityManager entityManager;
	 
    @Autowired
    public TravelDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

	@Override
	@Transactional
	public List<Flight> getAvailableFlights(String date) throws SQLException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date parsedDate = null;
		try {
			parsedDate = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		TypedQuery<Flight> query = entityManager
				.createNamedQuery("SELECT fl FROM flights_g_titan WHERE Departure_Date = :departureDate", Flight.class)
				.setParameter("departureDate", parsedDate, TemporalType.TIMESTAMP);
		List<Flight> flights = query.getResultList();
		return flights;
	}

	@Override
	@Transactional
	public Flight purchaceFlightTicket(String flight, int seat) throws SQLException {
		
		Query query = entityManager.createQuery(
			      "UPDATE flights_g_titan fl SET Seat_Availability = Seat_Availability + :seat" +
			      "WHERE fl.Flight_Code = :flight");
	    int updateCount = query.setParameter("price",seat).setParameter("flight", flight).executeUpdate();
	    Flight purchasedFlight = null;
	    if (updateCount > 0) { //if data was updated
		    TypedQuery<Flight> selectQuery = entityManager
								.createNamedQuery("SELECT fl FROM flights_g_titan WHERE Flight_Code = :flight", Flight.class)
								.setParameter("flight", flight);
		    purchasedFlight = selectQuery.getSingleResult();
		}
		return purchasedFlight;
	}

	@Override
	@Transactional
	public Flight addFlights(Flight flight) throws SQLException {
		entityManager.persist(flight);
		return flight;
	}

	@Override
	@Transactional
	public List<Flight> changeFlightPrice(String flight, double price) throws SQLException {
		
		Query query = entityManager.createQuery(
			      "UPDATE flights_g_titan fl SET Price = Price + :price" +
			      "WHERE fl.Flight_Code = :flight");
	    int updateCount = query.setParameter("price",price).setParameter("flight", flight).executeUpdate();
	    List<Flight> flights = new ArrayList<>();
	    if (updateCount > 0) { //if data was updated
		   TypedQuery<Flight> selectQuery = entityManager
							.createNamedQuery("SELECT fl FROM flights_g_titan WHERE Flight_Code = :flight", Flight.class)
							.setParameter("flight", flight);
		   flights = selectQuery.getResultList();
	    }
		return flights;
	}

	@Override
	@Transactional
	public List<Destination> getFlightDestinations(int airline) throws SQLException {
		
		TypedQuery<Object[]> query = entityManager.createQuery(
			    "SELECT DISTINCT ai.country, ai.city, ai.name " +
			    "FROM routes ro INNER JOIN airports ai " +
			    "ON ai.Airport_ID = ro.Destination_Airport_ID AND ro.Airline_ID = :airlineID"
			    , Object[].class).setParameter("airlineID", airline);

		List<Destination> destinations = new ArrayList<>();
		Destination destination;
		for (Object[] result: query.getResultList()) {
			destination = new Destination();
			destination.setCountry(result[0].toString());
			destination.setCity(result[1].toString());
			destination.setAirportName(result[2].toString());
			destinations.add(destination);
		}
		return destinations;
	}

}
