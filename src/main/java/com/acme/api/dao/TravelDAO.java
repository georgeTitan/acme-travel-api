package com.acme.api.dao;

import java.sql.SQLException;
import java.util.List;

import com.acme.api.domain.entity.Flight;
import com.acme.api.message.Destination;

public interface TravelDAO {
	
	public List<Flight> getAvailableFlights(String date) throws SQLException;
	public List<Destination> getFlightDestinations(int airline) throws SQLException;
	public Flight purchaceFlightTicket(String flight, int seat) throws SQLException;
	public Flight addFlights(Flight flight) throws SQLException;
	public List<Flight> changeFlightPrice(String flight, double price) throws SQLException;
}
