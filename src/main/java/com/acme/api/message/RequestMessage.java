package com.acme.api.message;

import java.util.List;

import com.acme.api.domain.entity.Flight;

/**
 * @author Georget
 *
 */
public class RequestMessage {
	
	private String requestType;
	private String timestamp;
	private List<Flight> flights;
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public List<Flight> getFlights() {
		return flights;
	}
	public void setFlights(List<Flight> flights) {
		this.flights = flights;
	}
}
