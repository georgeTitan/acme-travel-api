package com.acme.api.message;

public class Link {
	
	private String relation;
	private String href;
	
	public Link(String relation, String href) {
		this.relation = relation;
		this.href = href;
	}
	public String getRelation() {
		return relation;
	}
	
	public String getHref() {
		return href;
	}
}
