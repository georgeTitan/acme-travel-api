package com.acme.api.filter;

import java.util.List;
import java.util.Map;

import javax.xml.crypto.URIReferenceException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.mule.api.MuleMessage;
import org.mule.api.routing.filter.Filter;

import com.acme.api.domain.entity.Flight;

public class TravelDataFilter implements Filter {

	enum RequiredHTTPPostFields {
		flightCode, airlineName, departureAirport, destinationAirport, departureDate, aircraftType, seatAvailibility, price
	}

	@Override
	public boolean accept(MuleMessage message) {

		String httpMethod = message.getInboundProperty("http.method");
		String path = message.getInboundProperty("http.request.path");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		if ("post".equalsIgnoreCase(httpMethod) || "put".equalsIgnoreCase(httpMethod)) {

			@SuppressWarnings("unchecked")
			Map<String, Object> payloadMap = (Map<String, Object>) message.getPayload();

			@SuppressWarnings("unchecked")
			Map<String, String> flights = (Map<String, String>) payloadMap.get("flights");

			// check if all required fields are present with valid value
			for (RequiredHTTPPostFields required : RequiredHTTPPostFields.values()) {
				if (!flights.containsKey(required.toString())) {
					throw new NullPointerException(required + " is missing");
				}
			}
			try {
				// validate date formatted fields
				formatter.parse(flights.get("departureDate"));
				Integer.parseInt(flights.get("seatAvailibility"));
				Double.parseDouble(flights.get("price"));
			} catch (ParseException e) {
				throw new IllegalArgumentException("Departure date format must be yyyy-MM-dd hh:mm:ss."
						+ "seatAvailibility must be valid integer." + "price must be valid decimal");
			}
		} else { // http get method

			Map<String, String> queryParams = message.getInboundProperty("http.query.params");
			if (queryParams.containsKey("date")) {

				try {
					// validate date formatted fields
					formatter.parse(queryParams.get("date"));
				} catch (ParseException e) {
					throw new IllegalArgumentException("date format must be yyyy-MM-dd hh:mm:ss");
				}

			} else if (!queryParams.containsKey("airline")) {
				throw new NullPointerException("search request must contain either date or airline parameters");
			}
		}

		return true;
	}
}
