package com.acme.api.domain.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Georget
 *
 */
@Entity
@Table(name = "flights_g_titan")
public class Flight implements Serializable {
	@Id
	@Column(name = "Flight_Code")
	private String flightCode;
	@Id
	@Column(name = "Departure_Date")
	private String departureDate;
	
	@Column(name = "Airline_Name")
	@OneToMany(targetEntity = Airline.class, cascade = { CascadeType.ALL }, orphanRemoval = true)
	private Set<String> airlineName;
	@Column(name = "Departure_Airport")
	private String departureAirport;
	@Column(name = "Destination_Airport")
	private String destinationAirport;
	
	@Column(name = "Aircraft_Type")
	@OneToMany(targetEntity = Aircraft.class, cascade = { CascadeType.ALL }, orphanRemoval = true)
	private Set<String> aircraftType;
	@Column(name = "Seat_Availability")
	private int seatAvailibility;
	@Column(name = "Price")
	private double price;
	
	public String getFlightCode() {
		return flightCode;
	}
	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}
	public String getDepartureAirport() {
		return departureAirport;
	}
	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}
	public String getDestinationAirport() {
		return destinationAirport;
	}
	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}
	public String getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public int getSeatAvailibility() {
		return seatAvailibility;
	}
	public void setSeatAvailibility(int seatAvailibility) {
		this.seatAvailibility = seatAvailibility;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Set<String> getAirlineName() {
		return airlineName;
	}
	public void setAirlineName(Set<String> airlineName) {
		this.airlineName = airlineName;
	}
	public Set<String> getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(Set<String> aircraftType) {
		this.aircraftType = aircraftType;
	}

}
