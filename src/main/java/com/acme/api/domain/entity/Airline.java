package com.acme.api.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "airlines")
public class Airline implements Serializable {
	
	private enum ACTIVE_CODE {Y,N};
	
	@Id
	@Column(name = "Airline_ID")
	private int id;
	
	@Column(name = "Name")
	@Id
	private String name;
	
	@Column(name = "Alias")
	private String alias;
	
	@Column(name = "IATA")
	private String iata;
	
	@Column(name = "ICAO")
	private String icao;
	
	@Column(name = "Callsign")
	private String callSign;
	
	@Column(name = "Country")
	private String country;
	
	@Column(name = "Active")
	@Enumerated(EnumType.STRING)
	private ACTIVE_CODE active;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getIcao() {
		return icao;
	}

	public void setIcao(String icao) {
		this.icao = icao;
	}

	public String getCallSign() {
		return callSign;
	}

	public void setCallSign(String callSign) {
		this.callSign = callSign;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ACTIVE_CODE getActive() {
		return active;
	}

	public void setActive(ACTIVE_CODE active) {
		this.active = active;
	}
}
