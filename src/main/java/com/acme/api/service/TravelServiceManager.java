package com.acme.api.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.acme.api.dao.TravelDAO;
import com.acme.api.domain.entity.Flight;
import com.acme.api.message.Destination;
import com.acme.api.message.Link;
import com.acme.api.message.RequestMessage;
import com.acme.api.message.ResponseMessage;

/**
 * @author Georget
 *
 */
public class TravelServiceManager {
	
	private TravelDAO travelDAO;
	
	@Autowired
	public TravelServiceManager(TravelDAO travelDAO) {
		this.travelDAO = travelDAO;
	}
	
	/**
	 * @param date
	 * @return
	 * @throws Exception
	 */
	public ResponseMessage getAvailableFlights(final String date) throws Exception {
		
		List<Flight> flights = travelDAO.getAvailableFlights(date);
		ResponseMessage response = new ResponseMessage();
		response.setResponseType("getAvailableFlights");
		response.setStatus("200");
		response.setFlights(flights);
		response.setLinks(addHateoas(new HashMap<String, String>() {
			{
				put("list.of.destinations", "/flights/airline={airline}");
				put("ticket.purchase", "/flights/seat/");
				put("add.flight(s)", "/flights/");
				put("change.price", "/flights/price/");
			}
		}));
		return response;
	}
	
	/**
	 * @param airline
	 * @return
	 * @throws Exception
	 */
	public ResponseMessage getFlightDestinations(final int airline) throws Exception {
		
		List<Destination> destinations = travelDAO.getFlightDestinations(airline);
		ResponseMessage response = new ResponseMessage();
		response.setDestinations(destinations);
		response.setMessage("ok");
		response.setResponseType("getDestinations");
		response.setStatus("200");
		response.setLinks(addHateoas(new HashMap<String, String>() {
			{
				put("available.flights", "/flights/date={datetime}");
				put("ticket.purchase", "/flights/seat/");
				put("add.flight(s)", "/flights/");
				put("change.price", "/flights/price/");
			}
		}));
		return response;
	}
	
	/**
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public ResponseMessage purchaceFlightTicket(final RequestMessage message) throws Exception {
		
		Flight requestFlight = message.getFlights().get(0);
		Flight flight = travelDAO.purchaceFlightTicket(requestFlight.getFlightCode(), requestFlight.getSeatAvailibility());
		ResponseMessage response = new ResponseMessage();
		response.setFlights(Arrays.asList(flight));
		response.setMessage("ok");
		response.setResponseType("purchaseFlight");
		response.setStatus("200");
		response.setLinks(addHateoas(new HashMap<String, String>() {
			{
				put("list.of.destinations", "/flights/airline={airline}");
				put("available.flights", "/flights/date={datetime}");
				put("add.flight(s)", "/flights/");
				put("change.price", "/flights/price/");
			}
		}));
		return response;
	}
	
	/**
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public ResponseMessage addFlights(final RequestMessage message) throws Exception {
		
		Flight newFlight = travelDAO.addFlights(message.getFlights().get(0));
		ResponseMessage response = new ResponseMessage();
		response.setFlights(Arrays.asList(newFlight));
		response.setMessage("ok");
		response.setResponseType("addFlights");
		response.setStatus("200");
		response.setLinks(addHateoas(new HashMap<String, String>() {
			{
				put("list.of.destinations", "/flights/airline={airline}");
				put("available.flights", "/flights/date={datetime}");
				put("ticket.purchase", "/flights/seat/");
				put("change.price", "/flights/price/");
			}
		}));
		return response;
	}
	
	/**
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public ResponseMessage changeFlightPrice(final RequestMessage message) throws Exception {
		
		Flight requestFlight = message.getFlights().get(0);
		List<Flight> newFlights = travelDAO.changeFlightPrice(requestFlight.getFlightCode(), requestFlight.getPrice());
		ResponseMessage response = new ResponseMessage();
		response.setFlights(newFlights);
		response.setMessage("ok");
		response.setResponseType("changeFlightPrice");
		response.setStatus("200");
		response.setLinks(addHateoas(new HashMap<String, String>() {
			{
				put("list.of.destinations", "/flights/airline={airline}");
				put("available.flights", "/flights/date={datetime}");
				put("ticket.purchase", "/flights/seat/");
				put("add.flight(s)", "/flights/");
			}
		}));
		return response;
	}
	
	
	/**
	 * @param elements
	 * @return
	 */
	private List<Link> addHateoas(Map<String, String> elements) {
		
		List<Link> links = new ArrayList<>();
		Link link;
		for (Map.Entry<String, String> entry: elements.entrySet()) {
			link = new Link(entry.getKey(),entry.getValue());
			links.add(link);
		}
		return links;
	}

}
