package com.acme.api.service;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acme.api.message.RequestMessage;
import com.acme.api.message.ResponseMessage;

import javax.ws.rs.PathParam;

@Service
@Path("/flights")
public class TravelServiceResource {
	
	private TravelServiceManager travelServiceManager;
	
	@Autowired
	public TravelServiceResource (TravelServiceManager travelServiceManager) {
		this.travelServiceManager = travelServiceManager;
	}
	
	public TravelServiceResource() {}
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/date={date}")
    public ResponseMessage getAvailableFlights(@PathParam("date") String date) throws Exception {
        
		ResponseMessage responseMessage =  travelServiceManager.getAvailableFlights(date);
		return responseMessage;
    }
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/airline={airline}")
    public ResponseMessage getFlightDestinations(@PathParam("airline") int airline) throws Exception {
        
		ResponseMessage responseMessage =  travelServiceManager.getFlightDestinations(airline);
		return responseMessage;
    }
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/seat")
    public ResponseMessage purchaceFlightTicket(RequestMessage message) throws Exception {
		System.out.println("requested flight destinations ");
		ResponseMessage responseMessage =  travelServiceManager.purchaceFlightTicket(message);
		return responseMessage;
    }
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage addFlights(RequestMessage message) throws Exception {
		ResponseMessage responseMessage = travelServiceManager.addFlights(message);
		return responseMessage;
    }

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/price")
    public ResponseMessage changeFlightPrice(RequestMessage message) throws Exception {
        System.out.println("requested flight destinations ");
        ResponseMessage responseMessage = travelServiceManager.changeFlightPrice(message);
        return responseMessage;
    }
}
